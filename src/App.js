import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class Main_in extends Component{
  constructor(props){
    super(props);
    this.state = {
      mode : props.mode,
      personagens : []
    };
  }
  _onChange = ({target}) => {
    this.setState({
        [target.name] : target.value,
        mode : this.state.mode
    });
  }
  _onClick_1 = () => {
    axios.post("http://localhost:8080/insert?name=" + this.state.user).then(response => {
      alert(response.data);
    });
  }
  getPersonagens = (callback) => {
    axios.get("http://localhost:8080/all").then(response => {
      callback(response.data);
    });
  }
  _onClick_3 = () => {
    axios.post("http://localhost:8080/update?id=" + this.state.old_user + "&newName=" + this.state.new_user).then(response => {
      alert(response.data);
    });
  }
  _onClick_4 = () => {
    axios.delete("http://localhost:8080/delete?id=" + this.state.user).then(response => {
      alert(response.data);
    });
  }

  render() {
    if(this.props.mode === 1){
      return (
        <div className="cadastrar">
          <div className="titulo">
            <h2>
              Cadastrar Personagem
            </h2>
          </div>
          <div>
            <p>Nome:</p>
            <input type="text" name="user" onChange={this._onChange}></input>
          </div>
          <div className="buttons">
          <button onClick={this._onClick_1}><h3> Criar Personagem </h3></button>
          </div>
        </div>
      );
    }
    if(this.props.mode === 2){
      return (
        <div className="buscar">
          <div className="titulo">
            <h2>
              Listar Personagens
            </h2>
          </div>
          <div>
            <ul id="list_per"></ul>
            {
              this.getPersonagens((personagens) => {
                var ul = document.createElement('ul');
                document.getElementById('list_per').appendChild(ul);
                personagens.forEach((personagem) => {
                  var li = document.createElement('li');
                  ul.appendChild(li);
                  li.innerHTML += personagem.nome + ", ID: " + personagem.id;
                })
              })
            }
          </div>
        </div>
      );
    }
    if(this.props.mode === 3){
      return (
        <div className="editar">
          <div className="titulo">
            <h2>
              Editar Personagem
            </h2>
          </div>
          <div>
            ID:<input type="text" name="old_user" onChange={this._onChange}></input>
            Novo nome:<input type="text" name="new_user" onChange={this._onChange}></input>
          </div>
          <div className="buttons">
          <button onClick={this._onClick_3}><h3> Atualizar </h3></button>
          </div>
        </div>
      );
    }
    if(this.props.mode === 4){
      return (
        <div className="deletar">
          <div className="titulo">
            <h2>
              Apagar Personagem
            </h2>
          </div>
          <div>
            <p>ID:</p>
            <input type="text" name="user" onChange={this._onChange}></input>
          </div>
          <div className="buttons">
          <button onClick={this._onClick_4}><h3> Deletar </h3></button>
          </div>
        </div>
      );
    }
  }
}

function Selector (props) {
  if(props.option === 1){
    return(
      <div>
        <Main_in mode={1}/>
      </div>
    );
  }
  if(props.option === 2){
    return(
      <div>
        <Main_in mode={2}/>
      </div>
    );
  }
  if(props.option === 3){
    return(
      <div>
        <Main_in mode={3}/>
      </div>
    );
  }
  if(props.option === 4){
    return(
      <div>
        <Main_in mode={4}/>
      </div>
    );
  }
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      option : 1    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <button className="buttons" onClick={() => {this.setState({option:1})}}><h2>Cadastrar Personagem</h2></button>
            <button className="buttons" onClick={() => {this.setState({option:2})}}><h2>Listar Personagens</h2></button>
            <button className="buttons" onClick={() => {this.setState({option:3})}}><h2>Editar Personagem</h2></button>
            <button className="buttons" onClick={() => {this.setState({option:4})}}><h2>Apagar Personagem</h2></button>
          </div>
          <div><Selector option={this.state.option}/></div>
        </header>
      </div>
    );
  }
}

export default App;
